# require 'mina/bundler'
require 'mina/git'
require 'mina/rvm'    # for rvm support. (http://rvm.io)

set :domain, 'jamespdev.com'
set :deploy_to, '/tmp/mina-test'
set :repository, 'https://japellerano@bitbucket.org/japellerano/mina-test.git'
set :user, 'ubuntu'
set :identity_file, 'keys/ubuntu-ec2.pem'
# set :current_path, '/var/www/mina-test'

task :deploy do
  deploy do
    invoke :'git:clone'
  end
end

task :link do
  queue "sudo rm -rf /var/www/mina-test"
  queue "sudo ln -nfs #{deploy_to}/current/ /var/www/mina-test"
end